#ifndef PLATFORMINTERFACE_H
#define PLATFORMINTERFACE_H

#include "command.h"

class PlatformInterface
{
public:
    virtual ~PlatformInterface(){}
signals:
    virtual void sendCommand(const Command& cmd) = 0;
};

#endif // PLATFORMINTERFACE_H
