#include "datareader.h"

DataReader::DataReader(QObject *parent): Plugin(parent){}

QVector<CommandHandler> DataReader::init()
{
    return {
        {
            {CommandType::INSERT, "Tag"},
            this,
            [](const Command& cmd)
            {
                qDebug() << cmd.getData().toString();
            }
        }
    };
}
