#include "pluginmanager.h"

PluginManager::PluginManager(const QString& settingsPath, QObject *parent) :
    QObject(parent)
{
    loadPlugins(settingsPath);
}

void PluginManager::incomingCommand(const Command& cmd)
{
    qDebug() << __func__ << cmd.getType() << cmd.getData();
    QList<QPointer<Plugin>> plugins = m_pluginsHash.values(cmd.getType());
    for (int i = 0; i < plugins.size(); ++i)
    {
        QVector<CommandHandler> handlers = plugins[i]->init();
        for (CommandHandler handler : handlers)
        {
            if (handler.owner) handler.h(cmd);
        }
    }
}

void PluginManager::loadPlugins(const QString& settingsPath)
{
    // TODO: use multi-threading later
    QSettings settings(settingsPath, QSettings::IniFormat);
    qDebug() << "Settings path:" << settings.fileName();

    int numbOfPlugins = settings.beginReadArray("plugins");
    QPointer<Plugin> plugPtr;
    QString plugName;
    QVector<CommandHandler> plugHandlers;
    for (int i = 0; i < numbOfPlugins; ++i) {
        settings.setArrayIndex(i);
        plugName = settings.value("name").toString();
        plugPtr = getPlugin(plugName);
        if (!plugPtr)
        {
            qDebug() << "Failed to load plugin" << plugName;
            continue;
        }
        plugPtr->setParams(settings.value("params"));
        plugHandlers = plugPtr->init();
        connect(plugPtr, &Plugin::sendCommand,
                this, &PluginManager::sendCommand);
        for (int i = 0; i < plugHandlers.size(); ++i)
        {
            m_pluginsHash.insert(plugHandlers[i].type, plugPtr);
        }
        plugPtr->start();
        qDebug() << "Plugin" << plugName << "with params" << settings.value("params") << "is loaded";
    }
    settings.endArray();
}

QPointer<Plugin> PluginManager::getPlugin(const QString& name)
{
    // TODO: use QPluginLoader later
    static const QVector<QString> k_pluginsNames = {
        "DataReader",
        "DataGenerator"
    };
    if (name == k_pluginsNames[0]) return new DataReader();
    if (name == k_pluginsNames[1]) return new DataGenerator();
    return nullptr;
}
