#ifndef PLUGIN_H
#define PLUGIN_H

#include "command.h"
#include "commandtype.h"
#include "platforminterface.h"

#include <QObject>
#include <QPointer>
#include <QtPlugin>
#include <QVariant>

class CommandHandler
{
public:
    CommandType type;
    QPointer<QObject> owner;
    std::function<void(const Command&)> h;
};

class Plugin : public QObject, public PlatformInterface
{
    Q_OBJECT
public:
    explicit Plugin(QObject *parent = nullptr): QObject(parent){}
    virtual void setParams(const QVariant& params) = 0;
    virtual QVector<CommandHandler> init() = 0;
    virtual void start() = 0;
signals:
    void sendCommand(const Command& cmd);
};

#endif // PLUGIN_H
