#include "pluginmanager.h"

#include <QCoreApplication>
#include <QObject>

void testPluginManager()
{
    PluginManager pm("settings.ini");
    QObject::connect(&pm, &PluginManager::sendCommand,
                     &pm, &PluginManager::incomingCommand);
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    testPluginManager();

    return a.exec();
}
